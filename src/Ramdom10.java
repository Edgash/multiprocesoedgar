import java.util.Scanner;

public class Ramdom10 {

    public static void main(String[] args) {

        //pide un texto por pantalla
        Scanner sc = new Scanner(System.in);
        String i = sc.nextLine().toLowerCase();
        //pilla si la cadena de texto introducida es la palabra 'stop', si no lo es entra en el bucle
        while (!i.equals("stop")){
            //muestra un número random por pantalla menor o igual a 10
            System.out.println((int) (Math.random() * 11));
            //para que el scanner no detecte entre mayusculas y minusculas
            i = sc.nextLine().toLowerCase();
        }
    }
}