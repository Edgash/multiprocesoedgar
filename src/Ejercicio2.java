import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) throws IOException {
        //esta es la ruta del jar
        String command = "java -jar out/artifacts/Ramdom10_jar/Ramdom10.jar";
        List<String> argList = new ArrayList<>(Arrays.asList(command.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argList);
        //aquí está la ruta de donde guardamos el fichero
        BufferedWriter bw1 = new BufferedWriter(new FileWriter("randoms.txt"));
        String numeros = "";

        try{
            Process process = pb.start();
            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);
            //aquí creamos un scanner que te pide que escribas algo
            Scanner procesoSC = new Scanner(process.getInputStream());
            Scanner sc = new Scanner(System.in);
            //esta con el toLowCase te permite no distinguir mayusculas con minusculas (lo cual ayuda con el stop)
            String linea = sc.nextLine().toLowerCase();
        //esto compara que lo que hayas escrito no sea la palabra 'stop'
        while(!linea.equals("stop")){
            //estas líneas guardan la linea escrita
            bw.write(linea);
            bw.newLine();
            bw.flush();
            numeros = procesoSC.nextLine();
            //te muestra por pantalla el numero
            System.out.println(numeros);
            //esta guarda todas las lineas y evita que se sobreescriban las lineas y se guarde solo la última
            bw1.write(numeros+"\n");
            //no diferencia entre mayúsculas y minusculas
            linea = sc.nextLine().toLowerCase();
        }
        bw1.close();
        //detecta el error de fallo del proceso
        } catch (IOException e) {
            System.out.println("Fallo del proceso secundario");
        }
    }
}
